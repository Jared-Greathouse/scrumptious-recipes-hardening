from django.db import models
from django.conf import settings
from recipes.models import Recipe

USER_MODEL = settings.AUTH_USER_MODEL


# class Recipe(models.Model):
#     name = models.CharField(max_length=125)
#     author = models.ForeignKey(
#         USER_MODEL,
#         related_name="recipes",
#         on_delete=models.CASCADE,
#         null=True,
#     )
#     description = models.TextField()
#     image = models.URLField(null=True, blank=True)
#     created = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)

#     def __str__(self):
#         return self.name + " by " + str(self.author)


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    description = models.TextField()
    # image = models.URLField(null=True)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plan",
        on_delete=models.CASCADE,
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe",
        related_name="meal_plans",
    )

    def __str__(self):
        return self.name + " by " + str(self.owner)
