from django.contrib import admin

from meal_plans.models import Recipe, MealPlan


# class RecipeAdmin(admin.ModelAdmin):
#     pass


class MealPlanAdmin(admin.ModelAdmin):
    pass


# admin.site.register(Recipe, RecipeAdmin)
admin.site.register(MealPlan, MealPlanAdmin)
