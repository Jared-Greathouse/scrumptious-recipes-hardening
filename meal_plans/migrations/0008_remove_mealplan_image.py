# Generated by Django 4.0.3 on 2022-09-01 00:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0007_mealplan_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mealplan',
            name='image',
        ),
    ]
