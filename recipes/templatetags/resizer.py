from django import template

register = template.Library()


# def resize_to(value, arg):
#     print("value:", value)
#     print("arg:", arg)
#     return "resize done"


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    if servings is not None and target is not None:
        try:
            ratio = int(target) / int(servings)
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount


register.filter(resize_to)
